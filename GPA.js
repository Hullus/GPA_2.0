//1.მასწავლებლის ჟურნალი
let students = [

    studentJan = {
    name: "Jan",
    lastname: "Reno",
    age: 26,
    scores: {
        javaScript: 62,
        react: 57,
        python: 88,
        java: 90
    }
},

    studentKlod = {
    name: "Klod",
    lastname: "Mone",
    age: 19,
    scores: {
        javaScript: 77,
        react: 52,
        python: 92,
        java: 67,
    }
},


    studentVan = {
    name: "Van",
    lastname: "Gogh",
    age: 21,
    scores: {
        javaScript: 51,
        react: 98,
        python: 65,
        java: 70,
    }
},

    studentDam = {
    name: "Dam",
    lastname: "Square",
    age: 36,
    scores: {
        javaScript: 82,
        react: 53,
        python: 80,
        java: 65,
    }
},
];

let subjectCredits = {
    javaScript: 4,
    react: 7,
    python: 6,
    java: 3,
}
subjectCredits.sumCredits=    subjectCredits.javaScript +
                              subjectCredits.react +
                              subjectCredits.python +
                              subjectCredits.java;
//2.Student statistics

//Jan statistics
students[0].statistics = {}

 students[0].statistics.sum = students[0].scores.javaScript +
                              students[0].scores.react +
                              students[0].scores.python +
                              students[0].scores.java;


students[0].statistics.average = students[0].statistics.sum/4;

students[0].statistics.GPA =((1 * subjectCredits.javaScript) +
                             (0.5 * subjectCredits.react) +
                             (3 * subjectCredits.python) +
                             (3 * subjectCredits.java)) /
                              subjectCredits.sumCredits;


//Klod statistics

students[1].statistics = {}

students[1].statistics.sum = students[1].scores.javaScript +
                             students[1].scores.react +
                             students[1].scores.python +
                             students[1].scores.java;


students[1].statistics.average = students[1].statistics.sum/4;

students[1].statistics.GPA =((2 * subjectCredits.javaScript) +
                             (0.5 * subjectCredits.react) +
                             (4 * subjectCredits.python) +
                             (1 * subjectCredits.java)) /
                              subjectCredits.sumCredits;




//Van statistics

students[2].statistics = {}

students[2].statistics.sum = students[2].scores.javaScript +
                             students[2].scores.react +
                             students[2].scores.python +
                             students[2].scores.java;


students[2].statistics.average = students[2].statistics.sum/4;

students[2].statistics.GPA =((0.5 * subjectCredits.javaScript) +
                             (4 * subjectCredits.react) +
                             (2 * subjectCredits.python) +
                             (1 * subjectCredits.java))/
                              subjectCredits.sumCredits;


//Dam statistics
students[3].statistics = {}

students[3].statistics.sum = students[3].scores.javaScript +
    students[3].scores.react +
    students[3].scores.python +
    students[3].scores.java;


students[3].statistics.average = students[3].statistics.sum/4;

students[3].statistics.GPA =((3 * subjectCredits.javaScript) +
                             (0.5 * subjectCredits.react) +
                             (2 * subjectCredits.python) +
                             (1 * subjectCredits.java))/
                              subjectCredits.sumCredits;

console.log(students)
//3.Враг народа || Красный диплом

let classAverage = (students[0].statistics.average +
                    students[1].statistics.average +
                    students[2].statistics.average +
                    students[3].statistics.average)/ 4;

 students[0].statistics.average > classAverage ?  console.log("Ван Гог: Красный диплом") : console.log("Ван Гог: Враг народа");
 students[1].statistics.average > classAverage ?  console.log("Площадь плотины: Красный диплом") : console.log("Площадь плотины: Враг народа");
 students[2].statistics.average > classAverage ?  console.log("Жан Рено: Красный диплом") : console.log("Жан Рено: Враг народа");
 students[3].statistics.average > classAverage ?  console.log("Клод Моне: Красный диплом") : console.log("Клод Моне: Враг народа");


//4.პარტიის სიამაყე

if (students[0].statistics.GPA > students[1].statistics.GPA&&
    students[0].statistics.GPA > students[2].statistics.GPA&&
    students[0].statistics.GPA > students[3].statistics.GPA){

    console.log(`საუკეთესო GPA:${students[0].statistics.GPA} და ეს ეკუთვნის ${students[0].name}-ს!`)

} else if (students[1].statistics.GPA > students[0].statistics.GPA&&
           students[1].statistics.GPA > students[2].statistics.GPA&&
           students[1].statistics.GPA > students[3].statistics.GPA){

    console.log(`საუკეთესო GPA:${students[1].statistics.GPA} და ეს ეკუთვნის ${students[1].name}-ს!`)

} else if (students[2].statistics.GPA > students[0].statistics.GPA&&
           students[2].statistics.GPA > students[1].statistics.GPA&&
           students[2].statistics.GPA > students[3].statistics.GPA){

    console.log(`საუკეთესო GPA:${students[2].statistics.GPA} და ეს ეკუთვნის ${students[2].name}-ს!`)

} else if (students[3].statistics.GPA > students[0].statistics.GPA&&
           students[3].statistics.GPA > students[1].statistics.GPA&&
           students[3].statistics.GPA > students[2].statistics.GPA){

    console.log(`საუკეთესო GPA:${students[3].statistics.GPA} და ეს ეკუთვნის ${students[3].name}-ს!`)

} else{
    console.log("ვაი");
}

//5. ფრონტი ჩვენი არსობისა


if ((students[0].age > 21) &&
   ((students[0].statistics.average > students[1].statistics.average) &&
    (students[0].statistics.average > students[2].statistics.average) &&
    (students[0].statistics.average > students[3].statistics.average))) {

    console.log(`პენსიონერთა შორის საუკეთესოა:${students[0].name}`)

} else if((students[1].age > 21) &&
         ((students[1].statistics.average > students[0].statistics.average) &&
          (students[1].statistics.average > students[2].statistics.average) &&
          (students[1].statistics.average > students[3].statistics.average))) {

    console.log(`პენსიონერთა შორის საუკეთესოა:${students[1].name}`)

} else if((students[2].age > 21) &&
         ((students[2].statistics.average > students[0].statistics.average) &&
          (students[2].statistics.average > students[1].statistics.average) &&
          (students[2].statistics.average > students[3].statistics.average))) {

    console.log(`პენსიონერთა შორის საუკეთესოა:${students[2].name}`)

} else if((students[3].age > 21) &&
         ((students[3].statistics.average > students[0].statistics.average) &&
          (students[3].statistics.average > students[1].statistics.average) &&
          (students[3].statistics.average > students[2].statistics.average))) {

    console.log(`პენსიონერთა შორის საუკეთესოა:${students[3].name}`)

} else {
    console.log("ვაი")
}



//6. ფრონტი, არითმეტიკა და კლარნეტი

students[0].statistics.frontaverage = (students[0].scores.javaScript +students[0].scores.react)/2;
students[1].statistics.frontaverage = (students[1].scores.javaScript +students[1].scores.react)/2;
students[2].statistics.frontaverage = (students[2].scores.javaScript +students[2].scores.react)/2;
students[3].statistics.frontaverage = (students[3].scores.javaScript +students[3].scores.react)/2;



if (students[0].statistics.frontaverage > students[1].statistics.frontaverage &&
    students[0].statistics.frontaverage > students[2].statistics.frontaverage &&
    students[0].statistics.frontaverage > students[3].statistics.frontaverage){
    console.log(`სტუდენტ ${students[0].name + students[0].lastname} აქვს საუკეთესო ქულა :${students[0].statistics.frontaverage} `);
} else if (students[1].statistics.frontaverage > students[0].statistics.frontaverage &&
           students[1].statistics.frontaverage > students[2].statistics.frontaverage &&
           students[1].statistics.frontaverage > students[3].statistics.frontaverage){
    console.log(`სტუდენტ ${students[1].name + students[1].lastname} აქვს საუკეთესო ქულა :${students[1].statistics.frontaverage} `);
} else if (students[2].statistics.frontaverage > students[0].statistics.frontaverage &&
           students[2].statistics.frontaverage > students[1].statistics.frontaverage &&
           students[2].statistics.frontaverage > students[3].statistics.frontaverage){
    console.log(`სტუდენტ ${students[2].name + students[2].lastname} აქვს საუკეთესო ქულა :${students[2].statistics.frontaverage} `);
} else if (students[3].statistics.frontaverage > students[0].statistics.frontaverage &&
           students[3].statistics.frontaverage > students[1].statistics.frontaverage &&
           students[3].statistics.frontaverage > students[2].statistics.frontaverage){
    console.log(`სტუდენტ ${students[3].name + students[3].lastname} აქვს საუკეთესო ქულა :${students[3].statistics.frontaverage} `);
} else {
    console.log("ვაი")
}
